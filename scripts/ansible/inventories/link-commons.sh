#! /bin/bash

# ------------------------------------------------------------------------------
# Create links in each inventory to all YAML files found in `commons` directory.
# ------------------------------------------------------------------------------

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
INVENTORIES_PATH="$SCRIPT_DIR"
COMMONS_INVENTORY_PATH="$INVENTORIES_PATH/commons"



BOLD_RED='\e[1;31m'
BOLD_GREEN='\e[1;32m'
BOLD_YELLOW='\e[1;33m'

TXT_RESET='\e[0m'

function log_error() {
    local message="$1"
    echo -e "${BOLD_RED}${message}${TXT_RESET}"
}

function log_info() {
    local message="$1"
    echo -e "${BOLD_YELLOW}${message}${TXT_RESET}"
}

function log_debug() {
    local message="$1"
    echo -e "${TXT_RESET}${message}${TXT_RESET}"
}



function main() {
    log_info "Linking commons variables files"

    # shellcheck disable=SC2231
    find "$SCRIPT_DIR/commons" -regextype posix-extended -regex "^.+\\.(yml|yaml)$" -type f -print0 | while read -r -d $'\0' commons_yaml_file_path; do
        create_links_to_commons_yaml_file "$commons_yaml_file_path"
    done
}

function create_links_to_commons_yaml_file() {
    local commons_yaml_file_path="$1"
    local commons_yaml_file_relative_path
    commons_yaml_file_relative_path=$(realpath --relative-to "$COMMONS_INVENTORY_PATH" "$commons_yaml_file_path")

    log_info "Processing file '${commons_yaml_file_path}'"

    find "$SCRIPT_DIR" -mindepth 1 -maxdepth 1 -type d -print0 | while read -r -d $'\0' inventory_path; do
        create_link_to_commons_yaml_file "$commons_yaml_file_relative_path" "$inventory_path"
    done
}

function create_link_to_commons_yaml_file() {
    local commons_yaml_file_relative_path="$1"
    local inventory_path="$2"

    local yaml_link_path="${inventory_path}/${commons_yaml_file_relative_path}"
    local yaml_link_directory
    yaml_link_directory=$(dirname "$yaml_link_path")

    # do not process YAML files in the 'commons' inventory
    if [[ "$yaml_link_directory" =~ ^"$COMMONS_INVENTORY_PATH" ]]; then
        return
    fi

    # create parent directories if it does not exist
    if [[ ! -d "$yaml_link_directory" ]]; then
        log_debug "  Creating directory '${yaml_link_directory}'"
        mkdir -p "$yaml_link_directory"
    fi

    # find the relative path of the link target
    local yaml_link_target_path="commons/$commons_yaml_file_relative_path"
    local yaml_link_parent_path
    yaml_link_parent_path=$(dirname "$yaml_link_path")

    while [[ "$yaml_link_parent_path" != "$INVENTORIES_PATH" ]]; do
        yaml_link_target_path="../$yaml_link_target_path"
        yaml_link_parent_path=$(dirname "$yaml_link_parent_path")
    done

    # do not process existing files or wrong links
    if [[ -e "$yaml_link_path" ]]; then
        if [[ -L "$yaml_link_path" ]]; then
            if [[ "$(readlink "$yaml_link_path")" == "$yaml_link_target_path" ]]; then
                log_debug "  The link '${yaml_link_path}' does already exist"
            else
                log_error "  ERROR: The link '${yaml_link_path}' does already exist but its target is wrong"
                exit 1
            fi
        else
            log_error "  ERROR: The file '${yaml_link_path}' does already exist"
            exit 1
        fi
    else
        # create relative link
        log_debug "  Creating link '{yaml_link_path}' to '${yaml_link_target_path}'"
        ln -s "$yaml_link_target_path" "$yaml_link_path"
    fi
}

main "$@"
