import docker
import os
import requests
import testinfra.utils.ansible_runner

# Access to Ansible
ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE'])
# Access to Docker
docker_client = docker.from_env()
# Facts by host
hosts_facts = {}
# IP addresses by host
hosts_ips = {}

# -----
# Get the hosts whose features are tested.
tested_hosts = ansible_runner.get_hosts('tested')

for tested_host in tested_hosts:
    container = docker_client.containers.get(tested_host)
    tested_host_ip = container.attrs['NetworkSettings']['IPAddress']
    hosts_ips[tested_host] = tested_host_ip
    # WARNING: Facts gathering is slow! Activate only if really needed!
    # host_facts = ansible_runner.run(tested_host, 'setup')['ansible_facts']
    # hosts_facts[tested_host] = host_facts

# -----
# Get the hosts from which the features are tested.
# Use this if you really need to have a container acting as a client,
# but it's way better to directly test from your host...
#
# testing_hosts = ansible_runner.get_hosts('testing')
#
# for testing_host in testing_hosts:
#     container = docker_client.containers.get(testing_host)
#     testing_host_ip = container.attrs['NetworkSettings']['IPAddress']
#     hosts_ips[testing_host] = testing_host_ip
#     # WARNING: Facts gathering is slow! Activate only if really needed!
#     # host_facts = ansible_runner.run(testing_host, 'setup')['ansible_facts']
#     # hosts_facts[testing_host] = host_facts

# -----
# The tests are run against all hosts unless the test contains a 'testinfra_hosts' list.
# This list can contain hostnames or ansible groups.
testinfra_hosts = tested_hosts

# IMPORTANT: This Python script is executed on the host running Molecule.


# --------------------------------------------------------------- Internal -----
# Execute parts of the tests on the node being tested by Molecule.
# Very slow...

def test_httpd_service_is_running_and_enabled(host):
    # remotely executed on the tested node:
    service = host.service('apache2')
    assert service.is_running
    assert service.is_enabled


def test_httpd_service_is_listening(host):
    socket = host.socket("tcp://0.0.0.0:80")
    assert socket.is_listening


# --------------------------------------------------------------- External -----
# Execute the tests on the host running Molecule.
# Very fast!

def test_httpd_service_is_responding():
    for tested_host in tested_hosts:
        tested_host_url = f'http://{hosts_ips[tested_host]}/'
        response = requests.get(tested_host_url)
        assert response.status_code == 200
        assert "Apache HTTP server" in response.text
