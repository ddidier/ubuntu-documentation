
# Ubuntu installation tests

Create a virtual machine with VirtualBox using Ubuntu 22.04 desktop and `ddidier` as the default account.

Create a host private network and set the VM IP to `192.168.20.101` (for example).

Export this IP for convenience: `export UBUNTU_TEST_IP=192.168.20.101`.

Create a shared folder between `/var/tmp/` on the host and `/mnt/shared/` on the guest.

Install a SSH Server on the guest: `sudo apt install openssh-server`.

You can now connect with:

```bash
ssh -o IdentitiesOnly=yes -i ./scripts/id_rsa ddidier@${UBUNTU_TEST_IP}
```

The key permission must be set to `chmod 600 ./id_rsa`



## Shell scripts

Copy the scripts on the guest:

```bash
# on the host:
rm -rf /var/tmp/ubuntu-test
mkdir /var/tmp/ubuntu-test
cp -r ./scripts/shell/* /var/tmp/ubuntu-test
```

Use then from the guest:

```bash
rm -rf /var/tmp/ubuntu-test/
sudo cp -r /mnt/shared/ubuntu-test /var/tmp/
sudo chown -R ddidier:ddidier /var/tmp/ubuntu-test/
mkdir /var/tmp/snapshots/
/var/tmp/ubuntu-test/configuration-snapshot.sh take
```



## Ansible playbook

Do not forget to change the guest IP in `scripts/ansible/inventories/testing/hosts.ini`.

```bash
./scripts/ansible/bin/ansible-playbook testing scripts/ansible/playbooks/all.yml \
    --private-key=id_rsa  \
    --extra-vars "ansible_sudo_pass=some-f-pwd\!" \
    --ssh-extra-args "-o IdentitiesOnly=yes" \
    --skip-tags "excluded_on_virtualbox"
```

With only one tag:

```bash
./scripts/ansible/bin/ansible-playbook testing scripts/ansible/playbooks/all.yml \
    --private-key=id_rsa  \
    --extra-vars "ansible_sudo_pass=some-f-pwd\!" \
    --ssh-extra-args "-o IdentitiesOnly=yes" \
    --skip-tags "excluded_on_virtualbox" \
    --tags development_python
```
