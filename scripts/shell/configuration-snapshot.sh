#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

function help() {
    echo "Manage snapshots of configuration"
    echo "Usage: configuration-snapshot take|compare|help"
}

function main() {
    if [ "$#" -lt 1 ]; then
        echo "Missing command"
        help
        exit 1
    fi

    if [ "$1" == "take" ]; then
        "$SCRIPT_DIR/_configuration-snapshot-take.sh" "${@:2}"
    elif [ "$1" == "compare" ]; then
        "$SCRIPT_DIR/_configuration-snapshot-compare.sh" "${@:2}"
    elif [ "$1" == "help" ]; then
        help
    else
        echo "Invalid command"
        help
        exit 1
    fi
}

main "$@"

exit 0
