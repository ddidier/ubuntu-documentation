# A Makefile to help building Sphinx documentation.
# The original Sphinx Makefile is "Makefile-sphinx".

SHELL:=/usr/bin/env bash

.DEFAULT_GOAL := html

.PHONY: html
html:
	./bin/build-html.sh

.PHONY: pdf
pdf:
	./bin/build-pdf.sh

.PHONY: serve
serve:
	./bin/serve.sh

.PHONY: package
package: html
	./bin/package.sh

.PHONY: tidy
tidy:
	./bin/tidy.sh
