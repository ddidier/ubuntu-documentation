
#################################
**Stacer** optimizer & monitoring
#################################



Install `Stacer <https://oguzhaninan.github.io/Stacer-Web/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install stacer

   # or from the official PPA
   sudo add-apt-repository ppa:oguzhaninan/stacer
   sudo apt install stacer

.. todo:: Have a look at `BleachBit <https://www.bleachbit.org/>`_ when released for Ubuntu 20.04.
