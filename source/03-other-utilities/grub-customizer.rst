
######################
**Grub Customization**
######################

Pick one of these two ways.



Grub Customizer
===============

Grub Customizer is a graphical interface to configure the GRUB2/BURG settings and menu entries.

Install `Grub Customizer <https://launchpad.net/grub-customizer>`_:

.. code-block:: shell
   :linenos:

   sudo add-apt-repository -y ppa:danielrichter2007/grub-customizer
   sudo apt install -y grub-customizer

You may add a background image in the :menuselection:`Appearence Settings` tab.



Grub theme
==========

Copy the theme directory inside ``/boot/grub/themes/`` and change the ownership with:

.. code-block:: shell
   :linenos:

   chown -R root:root /boot/grub/themes/

Reference the theme:

.. code-block::
   :caption: /etc/default/grub
   :linenos:

   GRUB_GFXMODE=1920x1080,auto

   GRUB_THEME="/boot/grub/themes/Slaze-ddidier/theme.txt"

Add some metadata to the menu entries:

.. code-block::
   :caption: /etc/grub.d/10_linux
   :linenos:

   # Change the line 511 at the time of writing from:
   echo "submenu '$(gettext_printf "Advanced options for %s" "${OS}" | grub_quote)' \$menuentry_id_option 'gnulinux-advanced-$boot_device_id' {"
   # To:
   echo "submenu '$(gettext_printf "Advanced options for %s" "${OS}" | grub_quote)' -class ubuntu-advanced --class gnu-linux-advanced --class gnu-advanced \$menuentry_id_option 'gnulinux-advanced-$boot_device_id' {"
   #                                                                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::
   :caption: /etc/grub.d/30_uefi-firmware
   :linenos:

   # Change the line 40 at the time of writing from:
   menuentry '$LABEL' \$menuentry_id_option 'uefi-firmware' {
   # To:
   menuentry '$LABEL' --class uefi --class efi \$menuentry_id_option 'uefi-firmware' {
   #                  ^^^^^^^^^^^^^^^^^^^^^^^^

Apply these changes with ``sudo update-grub``.

Some references:

- `Grub2 theme tutorial <http://wiki.rosalab.ru/en/index.php/Grub2_theme_tutorial>`_
- `Material Design Icons <https://materialdesignicons.com/>`_
- `Grub theme <https://github.com/vinceliuice/grub2-themes>`__
- `Grub theme <https://github.com/mateosss/matter>`__
