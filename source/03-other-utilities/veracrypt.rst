
#############################
**VeraCrypt** disk encryption
#############################



Download then install `Veracrypt <https://www.veracrypt.fr/>`_:

.. code-block:: shell
   :linenos:

   VERACRYPT_VERSION=1.25.9
   wget https://launchpad.net/veracrypt/trunk/${VERACRYPT_VERSION}/+download/veracrypt-${VERACRYPT_VERSION}-Ubuntu-$(lsb_release -rs)-amd64.deb -O /tmp/veracrypt-${VERACRYPT_VERSION}.deb
   sudo gdebi -n /tmp/veracrypt-${VERACRYPT_VERSION}.deb
