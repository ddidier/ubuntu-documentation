
##########################
**Inkscape** vector editor
##########################



Install `Inkscape <https://inkscape.org/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y inkscape
