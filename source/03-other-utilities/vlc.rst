
####################
**VLC** media player
####################



Install VLC:

.. code-block:: shell
   :linenos:

   sudo apt install -y vlc



Integration
===========

To associate media files with VLC:

- directly use the files browser, or
- edit ``~/.config/mimeapps.list``, e.g. ``video/mp4=vlc.desktop`` (cf. :ref:`/base-utilities/gnome#files-associations`)



Hardware acceleration
=====================

Hardware acceleration should be automatically enabled.

If not, go to :menuselection:`Preferences --> Input/Codecs --> Codecs` :

- with an Intel GPU: ``Hardwar-accelerated decoding`` = ``VA-API video decoder``
- with a NVIDIA GPU: ``Hardwar-accelerated decoding`` = ``VDPAU video decoder``
