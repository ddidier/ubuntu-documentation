
#############################
**MComix** comic books reader
#############################



Install `MComix <https://sourceforge.net/projects/mcomix/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y mcomix
