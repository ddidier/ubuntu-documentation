
#######################
**Thunar Bulk Renamer**
#######################



Install `Thunar <https://docs.xfce.org/xfce/thunar/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y --no-install-recommends thunar thunar-media-tags-plugin
