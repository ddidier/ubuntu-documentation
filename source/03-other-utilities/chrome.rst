
######################
**Chrome** web browser
######################



Install Chrome:

.. code-block:: shell
   :linenos:

   wget -qO - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
   echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
   sudo apt update
   sudo apt install -y google-chrome-stable

Install some extensions:

- `Bitwarden <https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb>`__
- `NordVPN <https://chrome.google.com/webstore/detail/nordvpn-1-vpn-proxy-exten/fjoaledfpmneenckfbpdfhkmimnjocfa>`__
- `Tabs Outliner <https://chrome.google.com/webstore/detail/tabs-outliner/eggkanocgddhmamlbiijnphhppkpkmkl>`__
- `uBlock Origin <https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=fr>`__
