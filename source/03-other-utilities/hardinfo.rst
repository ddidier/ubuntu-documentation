
##############################
**HardInfo** hardware analyzer
##############################



HardInfo is a hardware analysis, system benchmark and report generator.

Install `HardInfo <http://hardinfo.org>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y hardinfo
