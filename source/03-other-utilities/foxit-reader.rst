

###########################
**Foxit Reader** PDF reader
###########################



.. warning:: Foxit Reader is `no longer maintained <http://forums.foxitsoftware.com/forum/portable-document-format-pdf-tools/foxit-reader/183039-foxit-reader-freeze-on-ubuntu-20-04>`__ on Linux.


Download then install `Foxit Reader <https://www.foxitsoftware.com/pdf-reader/>`_:

.. code-block:: shell
   :linenos:

   FOXIT_READER_VERSION_PATH=2.x/2.4
   FOXIT_READER_VERSION=2.4.4.0911
   wget http://cdn01.foxitsoftware.com/pub/foxit/reader/desktop/linux/$FOXIT_READER_VERSION_PATH/en_us/FoxitReader.enu.setup.$FOXIT_READER_VERSION.x64.run.tar.gz -O /tmp/FoxitReader.enu.setup.$FOXIT_READER_VERSION.x64.run.tar.gz
   tar xfz /tmp/FoxitReader.enu.setup.$FOXIT_READER_VERSION.x64.run.tar.gz
   sudo -H /tmp/FoxitReader.enu.setup.$FOXIT_READER_VERSION.x64.run
