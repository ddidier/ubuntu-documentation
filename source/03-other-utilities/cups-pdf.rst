
####################
**Cups PDF** printer
####################



Install Cups PDF:

.. code-block:: shell
   :linenos:

   sudo apt install -y cups-pdf

You may change the default output directory (``$HOME/PDF/``) for something else:

1. change the output directory in ``/etc/cups/cups-pdf.conf``:

  .. code-block:: shell
     :caption: /etc/cups/cups-pdf.conf
     :linenos:

     # before
     Out ${HOME}/PDF

     # after
     Out ${HOME}/Documents

3. restart the service:

  .. code-block:: shell
     :linenos:

     sudo systemctl restart cups
