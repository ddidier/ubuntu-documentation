
###########################
**CopyQ** clipboard manager
###########################



Install `CopyQ <https://hluk.github.io/CopyQ/>`_:

.. code-block:: shell
   :linenos:

   sudo add-apt-repository -y ppa:hluk/copyq
   sudo apt install -y copyq
