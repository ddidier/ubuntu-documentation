
#############
Miscellaneous
#############



Microsoft fonts
===============

.. code-block:: shell
   :linenos:

   echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
   sudo apt install -y ttf-mscorefonts-installer



Flameshot screenshot
====================

Install `Flameshot <https://flameshot.org/>`_:

.. code-block:: shell
   :linenos:

   sudo snap install flameshot



Warp file transfer
==================

Install `Warp <https://gitlab.gnome.org/World/warp>`_:

.. code-block:: shell
   :linenos:

   sudo flatpak install flathub app.drey.Warp
