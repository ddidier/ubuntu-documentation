
###################
**Python** language
###################



Ubuntu 22.04 comes with Python 3.10.

Install PIP et VirtualEnv for all users:

.. code-block:: shell
   :linenos:

   sudo apt install -y python3-pip python3-venv

Install VirtualEnv et VirtualEnvWrapper for the current user:

.. code-block:: shell
   :linenos:

   pip3 install virtualenv virtualenvwrapper



pyenv environments manager
==========================

Install `pyenv <https://github.com/pyenv/pyenv>`_:

.. code-block:: shell
   :linenos:

   curl https://pyenv.run | bash

Then the suggested build dependencies:

.. code-block:: shell
   :linenos:

   sudo apt install \
      build-essential curl libbz2-dev libffi-dev liblzma-dev libncursesw5-dev \
      libreadline-dev libsqlite3-dev libssl-dev libxml2-dev libxmlsec1-dev \
      llvm make tk-dev wget xz-utils zlib1g-dev



PyCharm editor
==============

Install `PyCharm <https://www.jetbrains.com/pycharm/>`_:

.. code-block:: shell
   :linenos:

   sudo snap install pycharm-community --classic
