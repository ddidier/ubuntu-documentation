
#######################
**JavaScript** language
#######################



NVM environnements manager
==========================

Install `NVM <https://github.com/nvm-sh/nvm>`_ as a normal user and not as ``root``:

.. code-block:: shell
   :linenos:

   NVM_VERSION=X.Y.Z
   curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh | bash



YARN package manager
====================

Install `YARN <https://yarnpkg.com/>`_:

.. code-block:: shell
   :linenos:

   sudo npm install -g yarn



WebStorm editor
===============

Install `WebStorm <https://www.jetbrains.com/webstorm/>`_:

.. code-block:: shell
   :linenos:

   sudo snap install webstorm --classic
