
#################
**Java** language
#################



Install a system-wide OpenJDK:

.. code-block:: shell
   :linenos:

   sudo apt install -y openjdk-18-jdk



SDKMAN!
=======

SDKMAN! is a tool for managing parallel versions of multiple Software Development Kits on most Unix based systems.

Install `SDKMAN <https://sdkman.io/>`_:

.. code-block:: shell
   :linenos:

   curl -s "https://get.sdkman.io" | bash

Install one or more versions of OpenJDK and Maven for the current user:

.. code-block:: shell
   :linenos:

   # for example
   sdk install java 11.0.5-tem
   sdk install java 17.0.3-tem
   sdk install maven 3.8.6



IntelliJ editor
===============

Install `IntelliJ IDEA <https://www.jetbrains.com/idea/>`_:

.. code-block:: shell
   :linenos:

   sudo snap install intellij-idea-ultimate --classic

Some useful plugins:

- `CheckStyle <https://plugins.jetbrains.com/plugin/1065-checkstyle-idea>`__
- `CSV <https://plugins.jetbrains.com/plugin/10037-csv>`__
- `Eclipse Code Formatter <https://plugins.jetbrains.com/plugin/6546-adapter-for-eclipse-code-formatter>`__
- `INI <https://plugins.jetbrains.com/plugin/6981-ini>`__
- `JPA Buddy <https://plugins.jetbrains.com/plugin/15075-jpa-buddy>`__
- `Key Promoter X <https://plugins.jetbrains.com/plugin/9792-key-promoter-x>`__
- `Makefile <https://plugins.jetbrains.com/plugin/9333-makefile-language>`__
- `Material Theme UI <https://plugins.jetbrains.com/plugin/8006-material-theme-ui>`__
- `PlantUML <https://plugins.jetbrains.com/plugin/7017-plantuml-integration>`__
- `Python <https://plugins.jetbrains.com/plugin/631-python>`__
- `Ruby <https://plugins.jetbrains.com/plugin/1293-ruby>`__
- `SonarLint <https://plugins.jetbrains.com/plugin/7973-sonarlint>`__
- `String Manipulation <https://plugins.jetbrains.com/plugin/2162-string-manipulation>`__
- `Terraform and HCL <https://plugins.jetbrains.com/plugin/7808-terraform-and-hcl>`__
- `Window Layout Manager <https://plugins.jetbrains.com/plugin/13005-window-layout-manager>`__



Eclipse editor
==============

Download and install `Eclipse <https://www.eclipse.org/downloads/>`_.

.. todo:: Some useful plugins
