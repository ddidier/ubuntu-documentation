
##########################
**DrawIO** diagrams editor
##########################



Install `DrawIO <https://github.com/jgraph/drawio-desktop>`_:

.. code-block:: shell
   :linenos:

   # using a DEB package
   DRAWIO_VERSION=X.Y.Z
   wget https://github.com/jgraph/drawio-desktop/releases/download/v${DRAWIO_VERSION}/drawio-amd64-${DRAWIO_VERSION}.deb -O /tmp/draw.io-amd64-${DRAWIO_VERSION}.deb
   sudo gdebi -n /tmp/draw.io-amd64-${DRAWIO_VERSION}.deb

   # or using a SNAP
   sudo snap install drawio


Add MIME type and file association:

.. code-block:: xml
   :caption: /usr/share/mime/packages/application-vnd-jgraph-mxfile.xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
       <mime-type type="application/vnd.jgraph.mxfile">
           <comment>Drawio Diagram</comment>
           <icon name="application-vnd.jgraph.mxfile"/>
           <glob-deleteall/>
           <glob pattern="*.drawio" case-sensitive="true"/>
       </mime-type>
   </mime-info>

Add ``application/vnd.jgraph.mxfile drawio`` to ``/etc/mime.types``, then:

.. code-block:: shell
   :linenos:

   update-mime-database /usr/share/mime

   cp /snap/drawio/current/meta/gui/icon.png /usr/share/icons/application-vnd.jgraph.mxfile.png

   update-icon-caches /usr/share/icons/
