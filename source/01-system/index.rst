
######
System
######



.. toctree::
   :maxdepth: 1
   :glob:

   ubuntu
   audio
   video
   input
   storage
   network
   energy
