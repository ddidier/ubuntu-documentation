
######
Ubuntu
######

This chapter describes the installation of Ubuntu on a Dell XPS 15 laptop.
This will be a dual boot setup with Windows 10 and Ubuntu 19.10.
Ubuntu will be installed using `LVM <https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)>`_
on top of a `LUKS <https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup>`_ encrypted partition.

More details may be found in the articles this chapter is adapted from:

- `AskUbuntu - Encrypted custom install <https://askubuntu.com/questions/918021/encrypted-custom-install>`_
- `Installer un Ubuntu chiffré avec LUKS, LVM et un partitionnement personnalisé <https://zestedesavoir.com/tutoriels/1653/installer-un-ubuntu-chiffre-avec-luks-lvm-et-un-partitionnement-personnalise>`_



Pre-installation
================

Boot Ubuntu from a Live OS and select the option :guilabel:`Try Ubuntu without installing`.

Partition your drive using your favorite tool, for example `GParted <https://gparted.org/>`_.
Create a small partition for the boot process that cannot be encrypted, otherwise you won't be able to boot.
Create another partition for your data that will be encrypted.

If your device is ``/dev/nvme0n1``, the partitions may look like this:

============== =================== ====== =========== ==========
Partition      Name                Size   Mount point Filesystem
============== =================== ====== =========== ==========
/dev/nvme0n1p1 EFI                 500 Mo             FAT32
/dev/nvme0n1p2 Windows 10 MSR      128 Mo
/dev/nvme0n1p3 Windows 10          300 Go             BitLocker
/dev/nvme0n1p4                     1 Go               NTFS
/dev/nvme0n1p5 Windows 10 Recovery   1 Go             NTFS
/dev/nvme0n1p6 Ubuntu Boot           1 Go /boot       EXT4
/dev/nvme0n1p7 Ubuntu              650 Go             LUCKS
============== =================== ====== =========== ==========

Create the encrypted partition:

.. code-block:: shell
   :linenos:

   sudo cryptsetup luksFormat /dev/nvme0n1p7
   sudo cryptsetup luksOpen   /dev/nvme0n1p7 hdcrypt  # <1>

   # <1> the name 'hdcrypt' is arbitrary

While not necessary, it's a good idea to fill your LUKS partition with zeros
so that the partition, in an encrypted state, is filled with random data:

.. code-block:: shell
   :linenos:

   sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
   sudo apt install -y pv  # <1>
   sudo sh -c 'exec pv -tprebB 16 m /dev/zero >"$1"' _ /dev/mapper/hdcrypt

   # <1> 'pv' is a progress bar indicator

Setup LVM on your encrypted partition:

.. warning:: The size of the Swap partition must be larger than the total amount of RAM if you want the system to be able to hibernate.

.. code-block:: shell
   :linenos:

   sudo pvcreate /dev/mapper/hdcrypt
   sudo vgcreate vgcrypt /dev/mapper/hdcrypt    # <1>

   sudo lvcreate -n lvswap -L 40g vgcrypt       # <2>
   sudo lvcreate -n lvroot -l 100%FREE vgcrypt  # <3>

   sudo mkswap /dev/mapper/vgcrypt-lvswap
   sudo mkfs.ext4 /dev/mapper/vgcrypt-lvroot

   # <1> the name 'vgcrypt' is arbitrary
   # <2> the name 'lvswap' is arbitrary
   # <3> the name 'lvroot' is arbitrary



Installation
============

You're now ready to install Ubuntu:

.. danger:: When the installation is complete, DO NOT REBOOT! Choose the option :guilabel:`Continue Testing`.

1. At the *"Installation type"* step of the install, choose the :guilabel:`Something else` option
2. Assign the partition ``/dev/mapper/vgcrypt-lvroot`` to  ``/`` using an ``ext4`` filesystem
3. Assign the partition ``/dev/mapper/vgcrypt-lvswap`` to the ``swap`` filesystem
4. Assign the partition ``/dev/nvme0n1p6`` to ``/boot`` using an ``ext4`` filesystem
5. Change the ``Device for boot loader installation`` to ``/dev/nvme0n1p6``
6. Continue with the installation but **DO NOT REBOOT**: choose the option :guilabel:`Continue Testing`



Post-installation
=================

Take note of the UUID (not the PARTUUID) of the encrypted partition ``/dev/nvme0n1p7``.
This can be found by typing in a terminal:

.. code-block:: shell
   :linenos:

   sudo blkid /dev/nvme0n1p7
   # /dev/nvme0n1p7: UUID="10b2c03f-69f4-49b8-8ca9-43f9ad665bc6" TYPE="crypto_LUKS" PARTLABEL="Ubuntu-18.04" PARTUUID="563a548a-e79f-41e7-8f6b-edb9b24c506f"

Mount the newly installed system:

.. code-block:: shell
   :linenos:

   sudo mount /dev/mapper/vgcrypt-lvroot /mnt/
   sudo mount /dev/nvme0n1p6             /mnt/boot
   sudo mount /dev/nvme0n1p1             /mnt/boot/efi

   sudo mount --bind  /dev  /mnt/dev
   sudo mount --bind  /proc /mnt/proc
   sudo mount --rbind /sys  /mnt/sys

   sudo chroot /mnt/

Declare the encrypted partition with ``crypttab``.
Create the file ``/etc/crypttab`` with the following content, replacing the UUID with the UUID of your disk:

.. code-block:: shell
   :caption: /etc/crypttab
   :linenos:

   # <target name>  <source device>                            <key file>  <options>
   hdcrypt          UUID=10b2c03f-69f4-49b8-8ca9-43f9ad665bc6  none        luks

.. note:: You may find some tutorials saying to:

   - change the boot loader configuration in ``/etc/default/grub``
   - add the configuration file ``/etc/initramfs-tools/conf.d/cryptroot``

   None of these steps are required.

Regenerate all the boot images and the boot loader configuration:

.. code-block:: shell
   :linenos:

   update-initramfs -k all -c
   update-grub

Exit the *chroot* environment with :kbd:`Control-D`.

Unmount the newly installed system:

.. code-block:: shell
   :linenos:

   sudo umount /mnt/sys /mnt/proc /mnt/dev /mnt/boot/efi /mnt/boot /mnt/



First boot
==========

Now you can reboot.

If the boot process get stuck at the LUKS password screen:

1. Reboot in :guilabel:`recovery mode`
2. Enter the LUKS password when requested
3. Resume normal boot with :guilabel:`resume`.

Save the LUKS header to a secure location in case it gets corrupted:

.. code-block:: shell
   :linenos:

   sudo cryptsetup luksHeaderBackup /dev/nvme0n1p7 --header-backup-file /luks-header.bin.crypt

You're finally ready to install the last updates and do some cleanup:

.. code-block:: shell
   :linenos:

   sudo apt update
   sudo apt upgrade
   sudo apt autoremove



How-to
======


How to `resize an encrypted partition <https://help.ubuntu.com/community/ResizeEncryptedPartitions>`_
-----------------------------------------------------------------------------------------------------

.. warning:: Backup all your data! There is a good chance something will go wrong...

Boot from a LiveCD then:

.. code-block:: shell
   :linenos:

   sudo apt update
   sudo apt install -y lvm2 cryptsetup

   sudo modprobe dm-crypt
   sudo cryptsetup luksOpen /dev/nvme0n1p7 vgcrypt
   sudo vgscan --mknodes
   sudo vgchange -ay

   sudo pvdisplay -m

   sudo cryptsetup resize vgcrypt
   sudo pvresize /dev/mapper/vgcrypt

   sudo pvchange -x y         /dev/mapper/vgcrypt
   sudo lvresize -l +100%FREE /dev/vgcrypt/lvroot
   sudo pvchange -x n         /dev/mapper/vgcrypt

   sudo e2fsck -f    /dev/vgcrypt/lvroot
   sudo resize2fs -p /dev/vgcrypt/lvroot


How to repair Grub with an encrypted partition from a LiveCD
------------------------------------------------------------

.. warning:: Backup all your data! There is some chance something will go wrong...

Boot from a LiveCD then:

.. code-block:: shell
   :linenos:

   sudo cryptsetup luksOpen /dev/nvme0n1p7 hdcrypt
   sudo vgscan
   sudo vgchange -ay
   sudo lvscan

   sudo mount /dev/mapper/vgcrypt-lvroot /mnt/
   sudo mount /dev/nvme0n1p6             /mnt/boot
   sudo mount /dev/nvme0n1p1             /mnt/boot/efi

   sudo mount --bind  /dev  /mnt/dev
   sudo mount --bind  /proc /mnt/proc
   sudo mount --rbind /sys  /mnt/sys

   sudo chroot /mnt/

   # repair with boot-repair, or with your bare hands ;-)
   grub-install /dev/nvme0n1
   grub-install --recheck /dev/nvme0n1
   update-grub

   # you may check the filesystem (see below)

   exit

   sudo umount /mnt/boot/efi /mnt/boot /mnt/dev /mnt/sys /mnt/proc /mnt/


How to check an encrypted partition from a LiveCD
-------------------------------------------------

.. warning:: Backup all your data!

Boot from a LiveCD then:

.. code-block:: shell
   :linenos:

   sudo cryptsetup luksOpen /dev/nvme0n1p7 hdcrypt
   sudo vgscan
   sudo vgchange -ay
   sudo lvscan

   sudo mount /dev/mapper/vgcrypt-lvroot /mnt/
   sudo mount /dev/nvme0n1p6             /mnt/boot
   sudo mount /dev/nvme0n1p1             /mnt/boot/efi

   sudo mount --bind  /dev  /mnt/dev
   sudo mount --bind  /proc /mnt/proc
   sudo mount --rbind /sys  /mnt/sys

   sudo chroot /mnt/

   fdisk -l
   umount /dev/mapper/vgcrypt-lvroot
   fsck -p /dev/mapper/vgcrypt-lvroot

   exit

   sudo umount /mnt/boot/efi /mnt/boot /mnt/dev /mnt/sys /mnt/proc /mnt/
