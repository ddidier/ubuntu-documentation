
#######
Storage
#######



exFAT driver
============

exFAT (Extended File Allocation Table) is a file system introduced by Microsoft in 2006 and optimized for flash memory such as USB flash drives and SD cards.

- The Linux kernels above version 5.4 natively support exFAT with an old driver.
- The Linux kernels above version 5.6 natively support exFAT with a new driver.
- Otherwise you will need this user space driver:

  .. code-block:: shell
     :linenos:

     sudo apt install -y exfat-fuse exfat-utils



CIFS driver
===========

CIFS stands for "Common Internet File System".
CIFS is a particular implementation of the Server Message Block (SMB) protocol, created by Microsoft.

.. code-block:: shell
   :linenos:

   sudo apt install -y cifs-utils



Partition editor
================

GParted is a free partition editor for graphically managing your disk partitions.

.. code-block:: shell
   :linenos:

   sudo apt install -y gparted
