
#####
Video
#####



Graphics drivers
================

Install NVidia drivers with the GUI:

1. Open :menuselection:`Software & Updates --> Additional Drivers`
2. Install the NVidia drivers
3. Reboot
4. Open :menuselection:`NVidia X Server Settings --> PRIME Profiles`
5. Select :guilabel:`Intel GPU` mode or :guilabel:`NVIDIA On-Demand` if you're feeling lucky
6. Reboot

Install NVidia drivers with the command line:

.. code-block:: shell
    :linenos:

    # automatic installation
    sudo ubuntu-drivers autoinstall

    # custom installation
    sudo apt install -y nvidia-driver-${NVIDIA_VERSION}

    # select the Intel mode
    /usr/bin/prime-select intel

In case you need a more recent version of the NVidia drivers:

.. code-block:: shell
    :linenos:

    sudo add-apt-repository -y ppa:graphics-drivers/ppa
    sudo apt install -y nvidia-driver-${NVIDIA_VERSION}

In case you need to check the hardware acceleration:

.. code-block:: shell
    :linenos:

    sudo apt install -y vainfo
    sudo vainfo



Screen brightness
=================

The screen of the Dell XPS 15 cannot be turned off with the function keys.

Use this command instead:

.. code-block:: shell
   :linenos:

   sudo echo 0 > /sys/class/backlight/intel_backlight/brightness
