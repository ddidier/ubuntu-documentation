
##############
Base Utilities
##############



.. toctree::
   :maxdepth: 1
   :glob:

   gnome

   bash
   zsh
   terminator
   command-line

   vim
   gedit
   sublime-text

   nautilus
   nemo

   firefox

   backintime
   clamav
   flatpak
