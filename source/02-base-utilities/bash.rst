
##############
**Bash** shell
##############



Customize the settings using `Bash-it <https://github.com/Bash-it/bash-it>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y git

   git clone --depth=1 https://github.com/Bash-it/bash-it.git $HOME/.bash_it
   $HOME/.bash_it/install.sh --silent
