
#############
**Zsh** shell
#############



Install Zsh:

.. code-block:: shell
   :linenos:

   sudo apt install -y zsh

Customize the settings using `Oh My Zsh <https://github.com/robbyrussell/oh-my-zsh>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y curl git
   sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

Customize the settings for ``root`` too using symbolic links (for a single user setup):

.. code-block:: shell
   :linenos:

    sudo ln -s ~ddidier/.zshrc     /root/.zshrc
    sudo ln -s ~ddidier/.oh-my-zsh /root/.oh-my-zsh
