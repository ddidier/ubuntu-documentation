
#################
**Gnome** desktop
#################



Gnome settings
==============

Customize the settings using the Gnome Settings dialog or the command line:

- :menuselection:`Wi-Fi`: configure as needed

- :menuselection:`Network`: configure as needed

- :menuselection:`Background --> Add Picture`: configure as needed

- :menuselection:`Appearance --> Desktop Icons --> Show personal folder` = ``Off``

  .. code-block:: shell

     gsettings org.gnome.shell.extensions.ding show-home false

- :menuselection:`Appearance --> Style` = ``Dark``

  .. code-block:: shell

     gsettings org.gnome.desktop.interface color-scheme 'prefer-dark'
     gsettings org.gnome.desktop.interface gtk-theme 'Yaru-dark'
     gsettings org.gnome.gedit.preferences.editor scheme 'Yaru-dark'

- :menuselection:`Appearance --> Configure dock behaviour --> Show Volumes and Devices` = ``Off``

  .. code-block:: shell

     gsettings org.gnome.shell.extensions.dash-to-dock show-mounts false

- :menuselection:`Appearance --> Configure dock behaviour --> Show Trash` = ``Off``

  .. code-block:: shell

     gsettings org.gnome.shell.extensions.dash-to-dock show-trash false

- :menuselection:`Privacy --> Thunderbolt`: authorize as needed (check the BIOS too)

- :menuselection:`Privacy --> Screen --> Screen Lock --> Blank Screen Delay` = ``3 minutes``

  .. code-block:: shell

     gsettings set org.gnome.desktop.session idle-delay 180

- :menuselection:`Privacy --> Screen --> Screen Lock --> Automatic Screen Lock` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.desktop.screensaver lock-enabled true

- :menuselection:`Privacy --> Screen --> Screen Lock --> Automatic Screen Lock Delay` = ``30 seconds``

  .. code-block:: shell

     gsettings set org.gnome.desktop.screensaver lock-delay 30

- :menuselection:`Privacy --> Screen --> Screen Lock --> Lock Screen On Suspend` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend true

- :menuselection:`Power --> Power Saving Options --> Automatic Suspend --> On Battery Power` = ``5 minutes``

  .. code-block:: shell

     gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'suspend'
     gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 300

- :menuselection:`Power --> Power Saving Options --> Automatic Suspend --> Plugged In` = ``20 minutes``

  .. code-block:: shell

     gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'suspend'
     gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 1200

- :menuselection:`Power --> Suspend & Power button --> Power Button Behaviour` : ``Hibernate``

  .. code-block:: shell

     gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'hibernate'

- :menuselection:`Power --> Suspend & Power button --> Show Battery Percentage` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.desktop.interface show-battery-percentage true

- :menuselection:`Displays --> Fractional Scaling` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.mutter experimental-features "['x11-randr-fractional-scaling']"

- :menuselection:`Displays --> Night Light` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true

     # 1. automatic schedule
     gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-automatic true

     # 2. manual schedule
     gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-automatic false
     gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 20.0
     gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 7.0

- :menuselection:`Users`: add a picture

- :menuselection:`Date & Time --> Automatic Date & Time` = ``On``

  .. code-block:: shell

     timedatectl set-ntp 1



Gnome tweaks
============

Install Gnome Tweaks to enable more customizations:

.. code-block:: shell

   sudo apt install -y gnome-tweaks

Customize the settings using the Gnome Tweaks dialog or the command line:

- :menuselection:`Appearence --> Background --> Image` : choose an image

  .. code-block:: shell

     cp Nebulous_Gem_3840x2160.jpg "$HOME/.local/share/backgrounds/"
     gsettings set org.gnome.desktop.background picture-uri "file://$HOME/.local/share/backgrounds/Nebulous_Gem_3840x2160.jpg"
     gsettings set org.gnome.desktop.background primary-color '#000000000000'
     gsettings set org.gnome.desktop.background secondary-color '#000000000000'

- :menuselection:`Appearence --> Lock Screen --> Image` : choose an image

  .. code-block:: shell

     cp Mustafarian_Sunrise_3840x2160.jpg "$HOME/.local/share/backgrounds/"
     gsettings set org.gnome.desktop.screensaver picture-uri "file://$HOME/.local/share/backgrounds/Mustafarian_Sunrise_3840x2160.jpg"
     gsettings set org.gnome.desktop.screensaver primary-color '#000000000000'
     gsettings set org.gnome.desktop.screensaver secondary-color '#000000000000'

- :menuselection:`Keyboard & Mouse --> Mouse Click Emulation` = ``Area``

  .. code-block:: shell

     gsettings set org.gnome.desktop.peripherals.touchpad click-method 'areas'

- :menuselection:`Top Bar --> Clock --> Weekday` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.desktop.interface clock-show-weekday true

- :menuselection:`Top Bar --> Calendar --> Week Numbers` = ``On``

  .. code-block:: shell

     gsettings set org.gnome.desktop.calendar show-weekdate true

.. TODO

    - :menuselection:`Window Titlebars --> Titlebar Buttons --> Placement` = ``Left``

      .. code-block:: shell

         gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'

- :menuselection:`Windows --> Attach Modal Dialogs` = ``Off``

  .. code-block:: shell

     gsettings set org.gnome.mutter attach-modal-dialogs false

- Enable *Click to Minimize*:

  .. code-block:: shell

     gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'

- Force :kbd:`Alt-Tab` to switch on current workspace only:

  .. code-block:: shell

     gsettings set org.gnome.shell.window-switcher current-workspace-only true
     gsettings set org.gnome.shell.app-switcher    current-workspace-only true



Gnome extensions
================

Install GNOME Extensions Manager:

.. code-block:: shell
   :linenos:

   sudo apt install -y gnome-shell-extension-manager

Install the following extensions
with the `command line <https://medium.com/@ankurloriya/install-gnome-extension-using-command-line-736199be1cda>`__
or with the GNOME Extensions Manager.

**Add Username to Top Panel**

Simply add your username to topbar panel aggregate menu.
[`Link <https://extensions.gnome.org/extension/1108/add-username-to-top-panel/>`__]

**Auto Move Windows**

Move applications to specific workspaces when they create windows.
[`Link <https://extensions.gnome.org/extension/16/auto-move-windows/>`__]

**Awesome Tiles**

Tile windows using keyboard shortcuts.
[`Link <https://extensions.gnome.org/extension/4702/awesome-tiles/>`__]

**Control the Blur Effect On Lock Screen**

Remove the blur effect on the lock screen.
[`Link <https://extensions.gnome.org/extension/2935/control-blur-effect-on-lock-screen/>`__]

**Date Menu Formater**

Customize the date and time format displayed in clock in the top bar in GNOME Shell.
[`Link <https://extensions.gnome.org/extension/4655/date-menu-formatter/>`__]

Settings:

- ``Pattern`` = ``EEEE d MMMM y - kk:mm``

.. code-block:: shell

   dconf write org/gnome/shell/extensions/date-menu-formatter/pattern "'EEEE d MMMM y - kk:mm'"

**GSConnect**

Connect to devices to securely share content like notifications or files and other features like SMS messaging and remote control.
[`Link <https://extensions.gnome.org/extension/1319/gsconnect/>`__]

.. TODO

    **Hibernate Status Button**

    Add a Hibernate button in the status menu. Using Alt modifier, you can also select Hybrid Sleep instead.
    [`Link <https://extensions.gnome.org/extension/755/hibernate-status-button/>`__]

**Night Theme Switcher**

Make your desktop easy on the eye, day and night.
Automatically toggle the color scheme between light and dark, switch backgrounds and run custom commands at sunset and sunrise.
[`Link <https://extensions.gnome.org/extension/2236/night-theme-switcher/>`__]

**Places Status Indicator**

Add a menu for quickly navigating places in the system.
[`Link <https://extensions.gnome.org/extension/8/places-status-indicator/>`__]

.. outdated

   **Put Windows**

   Fully customizable replacement for the old compiz put plugin.
   `Link <https://extensions.gnome.org/extension/39/put-windows/>`__]

**Refresh Wifi Connections**

This extension adds a refresh button to the Wifi connection selection dialog to request for a network scan manually.
[`Link <https://extensions.gnome.org/extension/905/refresh-wifi-connections/>`__]

**Remove App Menu**

Remove the Application Menu on the top bar.
[`Link <https://extensions.gnome.org/extension/3906/remove-app-menu/>`__]

**Sound Input & Output Device Chooser**

Shows a list of sound output and input devices (similar to gnome sound settings) in the status menu below the volume slider.
[`Link <https://extensions.gnome.org/extension/906/sound-output-device-chooser/>`__]

**Workspace Matrix**

Arrange workspaces in a two dimensional grid with workspace thumbnails.
[`Link <https://extensions.gnome.org/extension/1485/workspace-matrix/>`__]

Settings:

- ``Number of columns`` = ``2``
- ``Number of rows`` = ``2``
- ``Show workspace thumbnails`` = ``On``
- ``Show workspace grid in the overview`` = ``On``

.. code-block:: shell

   dconf write /org/gnome/shell/extensions/wsmatrix/num-columns '2'
   dconf write /org/gnome/shell/extensions/wsmatrix/num-rows '2'
   dconf write /org/gnome/shell/extensions/wsmatrix/show-overview-grid 'true'
   dconf write /org/gnome/shell/extensions/wsmatrix/show-thumbnails 'true'

**Pomodoro Timer**

.. code-block:: shell

   sudo apt install -y gnome-shell-pomodoro



.. _/base-utilities/gnome#files-associations:

Files associations
==================

Files associations are `quite a mess <https://wiki.archlinux.org/index.php/default_applications>`__!

The following files are used:

+---+-----------------------------------------------+-----------------------+
|   | Path                                          | Usage                 |
+===+===============================================+=======================+
| A | ``/etc/gnome/defaults.list``                  | Gnome defaults        |
+---+-----------------------------------------------+-----------------------+
| B | ``/usr/share/applications/defaults.list``     | Distribution defaults |
+---+-----------------------------------------------+-----------------------+
| C | ``~/.config/mimeapps.list``                   | User settings         |
+---+-----------------------------------------------+-----------------------+
| D | ``~/.local/share/applications/mimeapps.list`` | User settings         |
+---+-----------------------------------------------+-----------------------+

About user settings:

- `The official Gnome documentation <https://help.gnome.org/admin/system-admin-guide/stable/mime-types-application-user.html.en>`__ says to use the ``D`` file
- `The ArchLinux documentation <https://wiki.archlinux.org/index.php/XDG_MIME_Applications#mimeapps.list>`_ says that the ``D`` file is deprecated
- The ``D`` file doesn't exist anymore starting from Ubuntu 18.04

Some applications still use ``~/.local/share/applications/mimeapps.list``.

Although deprecated, several applications still read/write to file ``D``.
To simplify maintenance, simply create a symbolic link:

.. code-block:: shell

    ln -s ~/.config/mimeapps.list ~/.local/share/applications/mimeapps.list

.. The direction is important, the opposite doesn't work because of ``xdg-mime``.
   ln -s ~/.local/share/applications/mimeapps.list ~/.config/mimeapps.list



.. TODO

  ================================
  https://bluesabre.org/menulibre/
  ================================
