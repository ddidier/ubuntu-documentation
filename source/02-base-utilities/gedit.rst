
#####################
**GEdit** text editor
#####################



GEdit is installed by default.

Customize the settings using the preferences dialog or the command line:

- :menuselection:`View --> Display right margin at column` = ``120``

  .. code-block:: shell

     gsettings set org.gnome.gedit.preferences.editor display-right-margin true
     gsettings set org.gnome.gedit.preferences.editor right-margin-position 120

- :menuselection:`Editor --> Tab width` = ``4``

  .. code-block:: shell

     gsettings set org.gnome.gedit.preferences.editor tabs-size 4

- :menuselection:`Editor --> Insert spaces instead of tabs` = ``True``

  .. code-block:: shell

     gsettings set org.gnome.gedit.preferences.editor insert-spaces true

- :menuselection:`Plugins --> Sort` = ``True``

  .. code-block:: shell

      gsettings set org.gnome.gedit.plugins active-plugins "['docinfo', 'filebrowser', 'modelines', 'sort', 'spell', 'time']"
