
######################
**Nemo** files browser
######################



Since Nautilus has less and less features with each release, install Nemo:

.. code-block:: shell
   :linenos:

   sudo apt install -y nemo



Configuration
=============

Replace Nautilus with Nemo:

.. code-block:: shell
   :linenos:

   # open files and directories with Nemo
   xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search

   # changement de l'icône
   cp /usr/share/applications/nemo.desktop $HOME/.local/share/applications/
   sed -i 's/Icon=folder/Icon=org.gnome.Nautilus/' $HOME/.local/share/applications/nemo.desktop




Customize the settings using the preferences dialog or the command line:

- :menuselection:`Views --> Default View --> View new folders using` = ``List View``

  .. code-block:: shell

     gsettings set org.nemo.preferences default-folder-viewer 'list-view'

- :menuselection:`Views --> Tree View Defaults --> Show only folders` = ``False``

  .. code-block:: shell

     gsettings set org.nemo.sidebar-panels.tree show-only-directories false

- :menuselection:`Behavior --> Executable Text Files` = ``View executable text files when they are opened``

  .. code-block:: shell

     gsettings set org.nemo.preferences executable-text-activation 'display'

- :menuselection:`Display --> Window and Tab Titles --> Show the full path` = ``True``

  .. code-block:: shell

     gsettings set org.nemo.preferences show-full-path-titles true

- :menuselection:`Preview --> Previewable Files --> Show thumbnails` = ``Yes``

  .. code-block:: shell

     gsettings set org.nemo.preferences show-image-thumbnails 'always'

- :menuselection:`Preview --> Previewable Files --> Only for files smaller than` = ``10 MB``

  .. code-block:: shell

     gsettings set org.nemo.preferences thumbnail-limit 10485760



Extensions
==========

Install ``nemo-compare`` extension:

.. code-block:: shell
   :linenos:

   sudo apt install -y git meld nemo-python

   git -C /var/tmp/ clone https://github.com/linuxmint/nemo-extensions.git

   sudo mkdir /usr/share/nemo-compare/

   sudo cp /var/tmp/nemo-extensions/nemo-compare/src/*  /usr/share/nemo-compare/

   sudo ln -s /usr/share/nemo-compare/nemo-compare.py          /usr/share/nemo-python/extensions/nemo-compare.py
   sudo ln -s /usr/share/nemo-compare/nemo-compare-preferences /usr/bin/nemo-compare-preferences

   killall nemo


To open a new tab in an already running instance, instead of a new instance:

.. code-block:: diff
   :caption: /usr/share/nemo-compare/nemo-compare.py
   :linenos:

   diff -u /usr/share/nemo-compare/nemo-compare.py.bak /usr/share/nemo-compare/nemo-compare.py.dd
   --- /usr/share/nemo-compare/nemo-compare.py.bak
   +++ /usr/share/nemo-compare/nemo-compare.py.dd
   @@ -53,11 +53,11 @@

            cmd = None
            if len(paths) == 2:
   -            cmd = [self.config.diff_engine] + paths
   +            cmd = [self.config.diff_engine] + ["-n"] + paths
            elif len(paths) == 3 and len(self.config.diff_engine_3way.strip()) > 0:
   -            cmd = [self.config.diff_engine_3way] + paths
   +            cmd = [self.config.diff_engine_3way] + ["-n"] + paths
            elif len(self.config.diff_engine_multi.strip()) > 0:
   -            cmd = [self.config.diff_engine_multi] + paths
   +            cmd = [self.config.diff_engine_multi] + ["-n"] + paths

            if cmd is not None:
                GLib.spawn_async(argv=cmd, flags=GLib.SpawnFlags.DEFAULT | GLib.SpawnFlags.SEARCH_PATH)
