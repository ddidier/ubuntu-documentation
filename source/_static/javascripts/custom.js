
/*
 * Add an icon next to each external link.
 * A link is "external" if it begins with either 'http://' or 'https://'.
 */
function addIconToExternalLinks() {
    let image = $('<img />');
    image.addClass('external-link')
    image.attr('src', '/_static/images/external-link.svg');

    $('a[href^="http://"], a[href^="https://"]').not('a[class*=internal]')
        .addClass('external-link')
        .attr('target', '_blank')
        .append(image);
}

// -----------------------------------------------------------------------------

$(document).ready(function () {
    addIconToExternalLinks();
});
