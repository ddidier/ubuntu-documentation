
##############
**VirtualBox**
##############



Install `VirtualBox <https://www.virtualbox.org/>`_:

- using the Ubuntu package:

  .. code-block:: shell
     :linenos:

     export DEBIAN_FRONTEND=noninteractive
     sudo apt install -y virtualbox virtualbox-ext-pack virtualbox-guest-additions-iso

- using the VirtualBox package:

  .. code-block:: shell
     :linenos:

     echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
     wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
     wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc -O- | sudo apt-key add -
     sudo apt update
     sudo apt install -y virtualbox-6.1

You may install the extension pack:

.. code-block:: shell
   :linenos:

   VBOX_VERSION=$(vboxmanage --version | sed -E -e 's/r.+$//g' -e 's/_Ubuntu$//g')
   VBOX_EXTENSIONS_FILE="Oracle_VM_VirtualBox_Extension_Pack-$VBOX_VERSION.vbox-extpack"
   wget http://download.virtualbox.org/virtualbox/$VBOX_VERSION/$VBOX_EXTENSIONS_FILE -O /tmp/$VBOX_EXTENSIONS_FILE
   echo "y" | sudo VBoxManage extpack install --replace /tmp/$VBOX_EXTENSIONS_FILE

The usage of shared folders requires the user to have special permissions:

.. code-block:: shell
   :linenos:

   sudo usermod -a -G vboxusers $USER

You may add a network with :menuselection:`File --> Host Network Manager --> Create`.



CloneVDI
========

Download `CloneVDI <https://forums.virtualbox.org/viewtopic.php?t=22422>`_.

Install Wine:

.. code-block:: shell
   :linenos:

   sudo apt install -y wine64

Run CloneVDI with ``wine64 "/path/to/CloneVDI.exe"``.
