
##########
**Docker**
##########

.. warning:: Consider installing :doc:`/04-virtualization/podman` instead!



Install `Docker <https://www.docker.com/>`_:

.. code-block:: shell
   :linenos:

   wget -qO - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
   sudo apt update
   sudo apt install -y docker-ce docker-compose

You may allow a non-root user to run Docker:

.. code-block:: shell
   :linenos:

   sudo usermod -aG docker $USER

Enable the Docker service:

.. code-block:: shell
   :linenos:

   sudo systemctl enable docker



.. TEMP

   You may disable the Docker proxy by adding in ``/etc/docker/daemon.json``:

   .. code-block:: json
      :linenos:

      {
         "userland-proxy": false
      }

   Configure the firewall:

   .. code-block:: shell
      :linenos:

      sudo sed -i 's/DEFAULT_FORWARD_POLICY="DROP"/DEFAULT_FORWARD_POLICY="ACCEPT"/' /etc/default/ufw
