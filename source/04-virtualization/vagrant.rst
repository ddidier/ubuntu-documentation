
###########
**Vagrant**
###########



Download then install `Vagrant <http://www.vagrantup.com/>`_:

.. code-block:: shell
   :linenos:

   VAGRANT_VERSION=X.Y.Z
   wget https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.deb -O /tmp/vagrant_${VAGRANT_VERSION}_x86_64.deb
   sudo gdebi /tmp/vagrant_${VAGRANT_VERSION}_x86_64.deb
