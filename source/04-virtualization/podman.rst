
##########
**Podman**
##########



Install `Podman <https://podman.io/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y podman

Oh My Zsh does not have a Podman plugin yet:

.. code-block:: shell
   :linenos:

   mkdir -p $ZSH_CUSTOM/plugins/podman/
   podman completion zsh -f $ZSH_CUSTOM/plugins/podman/_podman
