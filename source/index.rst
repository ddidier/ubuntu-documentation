
###################
Ubuntu Installation
###################



My Ubuntu 22.04 setup...

.. note:: An Ansible playbook is provided in the ``script/ansible/`` directory.

.. toctree::
   :maxdepth: 2
   :numbered:

   01-system/index
   02-base-utilities/index
   03-other-utilities/index
   04-virtualization/index
   05-development/index
   06-games/index
