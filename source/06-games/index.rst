
#####
Games
#####



M.A.X.
======

M.A.X. Reloaded

.. code-block:: shell
   :linenos:

   sudo apt install -y libsdl2-mixer-2.0-0 libsdl2-net-2.0-0



Shadow
======

.. code-block:: shell
   :linenos:

   sudo apt install -y intel-media-va-driver-non-free libva-glx2
