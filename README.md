
# Ubuntu installation

Ubuntu installation, configuration and tips.

This documentation is available online at https://ddidier.gitlab.io/ubuntu-installation/.

This documentation can be generated with:

```bash
# non interactive
./bin/make-html -n ubuntu-installation

# interactive
./bin/make-livehtml -n ubuntu-installation
```
